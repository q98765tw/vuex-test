import { createStore } from "vuex";

export default createStore({
  state: {
    account: 0,
    item: [
      "大獎1",
      "大獎2",
      "大獎3",
      "大獎4",
      "大獎5",
      "垃圾",
      "垃圾",
      "垃圾",
      "垃圾",
      "垃圾",
    ],
    shuffled: [],
  },
  getters: {
    account: (state) => {
      return state.account;
    },
    shuffled: (state) => {
      return state.shuffled;
    },
  },
  mutations: {
    addCount(state) {
      state.account++;
    },
    reduceCount(state, num) {
      state.account = state.account - num;
    },
    random(state, num) {
      const shuffled = [...state.item].sort(() => 0.5 - Math.random());
      state.shuffled = shuffled.slice(0, num);
    },
  },
  actions: {
    addCount({ commit }) {
      commit("addCount");
    },
    reduceCount({ dispatch, commit }) {
      commit("reduceCount", 3);
      dispatch("random");
    },
    random({ commit }) {
      commit("random", 3);
    },
  },
  modules: {},
});
